package ro.tuc.pt.tema3.Model;

/** * Modeleaza atributele tabelului "Product" din baza de date a programului */
public class Product {
    /** ID-ul de identificare ap produsului */
    private Integer productId;
    /** Numele produsului */
    private String productName;
    /** Tara de origine a produsului */
    private String origin;

    /** Creaza un obiect instanta a clasei Product */
    public Product() {}

    /** Seteaza ID-ul produsului cu valoarea specificata
     * @param productId valoarea ID-ului */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** Seteaza numele produsului cu valoarea specificata
     * @param productName numele produsului */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /** Seteaza tara de origine a produsului cu valoarea specificata
     * @param origin tara de origine a produsului */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /** Returneaza ID-ul produsului
     *  @return ID-ul produsului */
    public Integer getProductId() {
        return productId;
    }

    /** Returneaza numele produsului
     * @return Numele produsului */
    public String getProductName() {
        return productName;
    }

    /** Returneaza tara de origine a produsului
     * @return Tara de origine a produsului */
    public String getOrigin() {
        return origin;
    }
}
