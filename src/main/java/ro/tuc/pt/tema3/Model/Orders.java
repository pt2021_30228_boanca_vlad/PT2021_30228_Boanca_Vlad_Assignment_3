package ro.tuc.pt.tema3.Model;

/** Modeleaza atributele tabelului "Orders" din baza de date a programului */
public class Orders {
    /** ID-ul comenzii */
    private Integer ordersId;
    /** ID-ul clientului care a efectuat comanda */
    private Integer clientId;
    /** ID-ul produsului care a fost comandat */
    private Integer productId;
    /** Cantitatea de produs comandata */
    private Integer quantity;
    /** Totalul de plata al comenzii*/
    private Double totalPrice;
    /** Creaza un obiect instanta a clasei Orders */
    public Orders() {}

    /** Returneaza ID-ul produsului care a fost comandat
     * @return ID-ul produsului comandat */
    public Integer getProductId() {
        return productId;
    }

    /** Returneaza ID-ul clientului care a efectuat comanda
     * @return ID-ul clientului */
    public Integer getClientId() {
        return clientId;
    }

    /** Returneaza ID-ul comenzii
     * @return ID-ul comenzii */
    public Integer getOrdersId() {
        return ordersId;
    }

    /** Returneaza totalul de plata al comenzii
     * @return Totalul de plata */
    public Double getTotalPrice() {
        return totalPrice;
    }

    /** Returneaza cantitatea de produs comandata
     * @return Cantitatea de produs comandata */
    public Integer getQuantity() {
        return quantity;
    }

    /** Seteaza ID-ul comenzii cu valoare specificata
     * @param ordersId ID-ul comenzii */
    public void setOrdersId(Integer ordersId) {
        this.ordersId = ordersId;
    }

    /** Seteaza ID-ul produsului comandat cu valoarea specificata
     * @param productId ID-ul produsului comandat */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /** Seteaza cantitatea de produs comandata cu valoarea specificata
     * @param quantity Cantitatea de produs comandata */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /** Seteaza ID-ul clientului cu valoarea specificata
     * @param clientId ID-ul clientului care a efectuat comanda*/
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /** Seteaza totalul de plata al comenzii cu valoarea specificata
     * @param totalPrice Totalul de plata al comenzii */
    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
