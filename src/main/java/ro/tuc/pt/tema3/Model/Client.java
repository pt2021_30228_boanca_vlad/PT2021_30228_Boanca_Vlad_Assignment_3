package ro.tuc.pt.tema3.Model;

/** Modeleaza atributele tabelului "Client" din baza de date a programului */
public class Client {
    /** ID-ul de identificare al clientului */
    private Integer clientId;
    /** Prenumele clientului */
    private String firstName;
    /** Numele de familie al clientului */
    private String lastName;
    /** Adresa clientului */
    private String address;

    /** Creaza un obiect instanta a clasei Client */
    public Client() {}

    /** Seteaza ID-ul clientului cu valoarea specificata de clientId
     * @param clientId valoarea ID-ului clientului */
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /** Seteaza prenumele clientului cu valoarea specificata de firstName
     * @param firstName prenumele clientului */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /** Seteaza numele clientului cu valoarea specificata de lastName
     * @param lastName numele clientului */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /** Seteaza adresa clientului cu valoarea specificata de address
     * @param address adresa clientului */
    public void setAddress(String address) {
        this.address = address;
    }

    /** Returneaza ID-ul clientului
     * @return ID-ul clientului */
    public Integer getClientId() {
        return clientId;
    }

    /** Returneaza prenumele clientului
     * @return prenumele clientului */
    public String getFirstName() {
        return firstName;
    }

    /** Returneaza numele clientului
     * @return numele clientului*/
    public String getLastName() {
        return lastName;
    }

    /** Returneaza adresa clientului
     * @return adresa clientului */
    public String getAddress() {
        return address;
    }
}

