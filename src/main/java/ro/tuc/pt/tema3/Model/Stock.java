package ro.tuc.pt.tema3.Model;

/**
 * Modeleaza atributele tabelului "Stock" din baza de date a programului
 */
public class Stock {
    /** ID-ul produsului al carui stock este descris */
    private Integer stockId;
    /** Cantitatea de produs disponibila */
    private Integer quantity;
    /** Pretul produsului*/
    private Double price;
    /** Creaza un obiect instanta a clasei Stock */
    public Stock() {}

    /** Returneaza ID-ul produsului al carui stock este descris
     * @return ID-ul produsului */
    public Integer getStockId() {
        return stockId;
    }

    /** Returneaza cantiatea de produs disponibila
     * @return Cantiatea de produs disponibila */
    public Integer getQuantity() {
        return quantity;
    }

    /** Returneaza pretul produsului
     * @return Pretul produsului */
    public Double getPrice() {
        return price;
    }

    /** Seteaza ID-ul produsului al carui stoc este descris cu valoarea specificata
     * @param stockId Valoarea ID-ului */
    public void setStockId(Integer stockId) {
        this.stockId = stockId;
    }

    /** Seteaza cantitatea de produs disponibila cu valoarea specificata
     * @param quantity Cantitatea de produs disponibila */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /** Seteaza pretul produsului cu valoarea specificata
     * @param price Pretul produsului */
    public void setPrice(Double price) {
        this.price = price;
    }
}
