package ro.tuc.pt.tema3.Presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Clasa care extinde clasa JFrame, folosita pentru crearea ferestrelor cu mesaje de informare sau de eroare. */
public class WarningFrame extends JFrame {
    /** Creaa un obiect instanta a clasei WarningFrame, carea fiseaza mesajul primit ca si parametru.
     * @param message Mesajul dorit pentru afisare */
    public WarningFrame(String message) {
        this.setTitle("Warning");
        this.setSize(300, 150);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setResizable(false);
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        JButton button = new JButton("OK");
        JTextArea msg = new JTextArea(2, 20);
        msg.setText(message);
        msg.setLineWrap(true);
        msg.setWrapStyleWord(true);
        msg.setEditable(false);
        msg.setFocusable(false);
        msg.setBackground(new Color(0, 0, 0, 0));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        mainPanel.add(msg);
        mainPanel.add(button);
        this.setContentPane(mainPanel);
        this.setVisible(true);
    }
}
