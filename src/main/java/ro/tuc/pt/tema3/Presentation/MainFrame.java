package ro.tuc.pt.tema3.Presentation;
import ro.tuc.pt.tema3.DAO.AbstractDAO;
import ro.tuc.pt.tema3.DAO.ClientDAO;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/** Creaza fereastra principala a interfetei grafice. Prin intermediul acesteia se pot face operatii asupra tabelelor din baza de date. */
public class MainFrame extends JFrame {
    private Controller controller;
    private JPanel clientOperations;
    private JPanel productOperations;
    private JPanel orderOperations;
    private JPanel stockOperations;
    private JScrollPane clientPane;
    private JScrollPane productPane;
    private JScrollPane orderPane;
    private JScrollPane stockPane;
    private JTable clientTable;
    private JTable productTable;
    private JTable orderTable;
    private JTable stockTable;
    private JButton addClient;
    private JButton editClient;
    private JButton deleteClient;
    private JButton addProduct;
    private JButton editProduct;
    private JButton deleteProduct;
    private JButton editStock;
    private JButton addOrder;
    private JButton deleteOrder;
    private Integer selectedRow;
    private ArrayList<Object> fields;

    /** Creaza un obiect instanta a clasei MainFrame.
     * @throws SQLException
     * @throws IllegalAccessException */
    public MainFrame() throws SQLException, IllegalAccessException {
        this.setTitle("Order management");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 550);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        initialize();
        watchInterface();
        JTabbedPane windows = new JTabbedPane();
        windows.setForeground(Color.black);
        windows.add("Clients", clientOperations);
        windows.add("Products", productOperations);
        windows.add("Stocks", stockOperations);
        windows.add("Orders", orderOperations);
        this.setContentPane(windows);
        this.setVisible(true);
    }

    /** Initializeaza variabilele folosite in construirea interfetei si creaza tabelele corespunzatoare din baza de date.
     * @throws SQLException
     * @throws IllegalAccessException */
    private void initialize() throws SQLException, IllegalAccessException {
        selectedRow = 1;
        fields = new ArrayList<>();
        controller = new Controller();
        clientOperations = new JPanel();
        productOperations = new JPanel();
        orderOperations = new JPanel();
        stockOperations = new JPanel();
        addClient = new JButton("Add new client");
        editClient = new JButton("Edit selected client");
        deleteClient = new JButton("Delete selected client");
        addProduct = new JButton("Add new product");
        editProduct = new JButton("Edit selected product");
        deleteProduct = new JButton("Delete selected product");
        editStock = new JButton("Edit stock for selected product");
        addOrder = new JButton("Place an order");
        deleteOrder = new JButton("Delete selected order");
        updateTable("Client", null, clientOperations, clientPane, addClient, editClient, deleteClient);
        updateTable("Product", null, productOperations, productPane, addProduct, editProduct, deleteProduct);
        updateTable("Stock", null, stockOperations, stockPane, editStock);
        updateTable("Orders", null, orderOperations, orderPane, addOrder, deleteOrder);
    }

    /** Adauga ActionListener pentru toate butoanele din interfata grafica. */
    private void watchInterface() {
        watchButton(addClient);
        watchButton(addProduct);
        watchButton(addOrder);
        watchButton(editClient);
        watchButton(editProduct);
        watchButton(editStock);
        watchButton(deleteClient);
        watchButton(deleteProduct);
        watchButton(deleteOrder);
    }

    /** In functie de butonul trimis ca parametru, acestuia ii este asociat un ActionListener corespunzator.
     * @param button Obiect din clasa JButton */
    private void watchButton(final JButton button) {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    switch (button.getText()) {
                        case "Add new client": watchInsertClientFrame(new EditFrame(controller.getClientDAO())); break;
                        case "Add new product": watchInsertProductFrame(new EditFrame(controller.getProductDAO())); break;
                        case "Place an order": watchInsertOrderFrame(new EditFrame(selectedRow, controller.getClientDAO(), controller.getProductDAO(), controller.getStockDAO(), controller.getOrdersDAO())); break;
                        case "Edit selected client": watchEditClientFrame(new EditFrame(selectedRow, controller.getClientDAO())); break;
                        case "Edit selected product": watchEditProductFrame(new EditFrame(selectedRow, controller.getProductDAO())); break;
                        case "Edit stock for selected product": watchEditStockFrame(new EditFrame(selectedRow, controller.getStockDAO())); break;
                        case "Delete selected client": tableAction(null, TableType.CLIENTS_TABLE, "delete"); break;
                        case "Delete selected product": tableAction(null, TableType.PRODUCTS_TABLE, "delete"); break;
                        case "Delete selected order": tableAction(null, TableType.ORDERS_TABLE, "delete"); break;
                    }
                } catch (SQLException throwables) {
                    new WarningFrame(throwables.getMessage());
                }
            }
        });
    }

    /** Salveaza in variabila selectedRow ID-ul elementului din tabel selectat de utilizator.
     * @param table Obiect de tipul JTable reprezentand tabelul dorit */
    private void watchTable(final JTable table) {
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                selectedRow = (Integer)table.getValueAt(table.getSelectedRow(), 0);
            }});
    }

    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru editarea unui client.
     * @param editFrame Fereastra de editare*/
    private void watchEditClientFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fields.add(selectedRow);
                addClientFields(editFrame);
                tableAction(editFrame, TableType.CLIENTS_TABLE, "update");
            }});
    }
    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru editarea unui produs.
     * @param editFrame Fereastra de editare*/
    private void watchEditProductFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fields.add(selectedRow);
                addProductFields(editFrame);
                tableAction(editFrame, TableType.PRODUCTS_TABLE, "update");
            }});
    }
    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru editarea unui stoc.
     * @param editFrame Fereastra de editare*/
    private void watchEditStockFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fields.add(selectedRow);
                try {
                    addStockFields(editFrame);
                } catch (SQLException throwable) {
                    new WarningFrame(throwable.getMessage());
                }
                tableAction(editFrame, TableType.STOCKS_TABLE, "update");
            }
        });
    }
    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru inserarea unui produs.
     * @param editFrame Fereastra de editare*/
    private void watchInsertClientFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getIndexValue(clientTable);
                addClientFields(editFrame);
                tableAction(editFrame, TableType.CLIENTS_TABLE, "insert");
            }});
    }
    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru adaugarea unui produs.
     * @param editFrame Fereastra de editare*/
    private void watchInsertProductFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getIndexValue(productTable);
                addProductFields(editFrame);
                tableAction(editFrame, TableType.PRODUCTS_TABLE, "insert");
            }});
    }
    /** Adauga un ActionListener butonului de submit din fereastra de editare, pentru plasarea unei comenzi.
     * @param editFrame Fereastra de editare*/
    private void watchInsertOrderFrame(final EditFrame editFrame) {
        editFrame.getSubmitButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getIndexValue(orderTable);
                addOrderFields(editFrame);
                tableAction(editFrame, TableType.ORDERS_TABLE, "insert");
            }});
    }

    /** Actualizraza informatiile din interfata grafica cu informatiile obtinute din baza de date.
     * @param tableName Numele tabelului in baza de date
     * @param editFrame Obiect din clasa EditFrame reprezentand fereastra de editare
     * @param panel Obiect din clasa JPanel reprezentand panoul care contine obiectul JScrollPanel
     * @param scrollPane Obiect din clasa JScrollPanel reprezentand panoul care contine panoul
     * @param buttons Obiecte din clasa JButton reprezentand butoanele pentru operatiile asupra tabelului corespunzator
     * @throws SQLException
     * @throws IllegalAccessException */
    private void updateTable(String tableName, EditFrame editFrame, JPanel panel, JScrollPane scrollPane, JButton ...buttons) throws SQLException, IllegalAccessException {
        switch (tableName) {
            case "Client":
                clientTable = controller.getTableFor(tableName);
                scrollPane = new JScrollPane(clientTable);
                watchTable(clientTable); break;
            case "Product":
                productTable = controller.getTableFor(tableName);
                scrollPane = new JScrollPane(productTable);
                watchTable(productTable); break;
            case "Stock":
                stockTable = controller.getTableFor(tableName);
                scrollPane = new JScrollPane(stockTable);
                watchTable(stockTable); break;
            case "Orders":
                orderTable = controller.getTableFor(tableName);
                scrollPane = new JScrollPane(orderTable);
                watchTable(orderTable); break;
        }
        panel.removeAll();
        panel.add(scrollPane);
        for(JButton button : buttons)
            panel.add(button);
        if(editFrame != null)
            editFrame.dispose();
    }

    /** Efectueaza operatii asupra tabelului, in functie de parametrii introdusi la apelul functiei.
     * @param editFrame Fereastra de editare.
     * @param tableType Tipul tabelului
     * @param operation Operatia dorita */
    private void tableAction(EditFrame editFrame, TableType tableType, String operation) {
        try {
            AbstractDAO dao = chooseDAO(tableType);
            chooseOperation(editFrame, dao, tableType, operation);
            update(editFrame, tableType);
        } catch (SQLException | InstantiationException | IllegalAccessException | IOException | IllegalArgumentException throwable) {
            new WarningFrame(throwable.getMessage());
        } finally { fields.clear(); }
    }

    /** Initializeaza variabila de tipul AbstractDAO transmisa ca parametru cu obiectul corespunzator.
     * @param tableType Tipul tabelului */
    private AbstractDAO chooseDAO(TableType tableType) {
        switch (tableType) {
            case CLIENTS_TABLE: return controller.getClientDAO();
            case PRODUCTS_TABLE: return controller.getProductDAO();
            case STOCKS_TABLE: return controller.getStockDAO();
            case ORDERS_TABLE: return controller.getOrdersDAO();
            default: return controller.getClientDAO();
        }
    }

    /** Executa operatiile necesare asupra tabelelor, in functie de operatia dorita.
     * @param editFrame Fereastra de ditare
     * @param dao Obiect dintr-o clasa care mosteneste clasa AbstractDAO
     * @param tableType Tipul tabelului
     * @param operation Operatia dorita
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws InstantiationException */
    private void chooseOperation(EditFrame editFrame, AbstractDAO dao, TableType tableType, String operation) throws SQLException, IllegalAccessException, InstantiationException, IOException {
        switch (operation) {
            case "insert":
                if(tableType.equals(TableType.PRODUCTS_TABLE)) {
                    fields.clear(); getIndexValue(stockTable); fields.add(0); fields.add(0.0);
                    controller.getStockDAO().add(fields);
                }
                if(tableType.equals(TableType.ORDERS_TABLE)) {
                    ArrayList<Object> aux = new ArrayList<>();
                    aux.addAll(fields);
                    fields.clear();
                    int a = ((Integer)controller.getStockDAO().getRowWithIndex(editFrame.getOrderProductField()).get(0));
                    fields.add(a);
                    fields.add((Integer)controller.getStockDAO().getRowWithIndex(editFrame.getOrderProductField()).get(1) - editFrame.getOrderQuantityField());
                    fields.add(controller.getStockDAO().getRowWithIndex(editFrame.getOrderProductField()).get(2));
                    controller.getStockDAO().update(fields);
                    controller.generateBill(editFrame);
                    fields.clear();
                    fields.addAll(aux);
                }
                dao.add(fields);
                break;
            case "update": dao.update(fields); break;
            case "delete": dao.delete(selectedRow); break;
        }
    }

    /** Actualizeaza tabelele din interfata grafica cu informatiile actualizate din baza de date.
     * @param editFrame Fereastra de editare din care s-a initializat operatia anterioara
     * @param tableType Tipul Tabelului
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IOException
     */
    private void update(EditFrame editFrame, TableType tableType) throws SQLException, IllegalAccessException, IOException {
        switch (tableType) {
            case CLIENTS_TABLE: updateTable("Client", editFrame, clientOperations, clientPane, addClient, editClient, deleteClient); break;
            case PRODUCTS_TABLE: updateTable("Stock", editFrame, stockOperations, stockPane, editStock); updateTable("Product", editFrame, productOperations, productPane, addProduct, editProduct, deleteProduct); break;
            case STOCKS_TABLE: updateTable("Stock", editFrame, stockOperations, stockPane, editStock); break;
            case ORDERS_TABLE: updateTable("Orders", editFrame, orderOperations, orderPane, addOrder, deleteOrder); updateTable("Stock", editFrame, stockOperations, stockPane, editStock); break;
        }
    }

    /** Adauga in fereastra de editare campurile necesare pentru editarea sau adaugarea unui client.
     * @param editFrame Fereastra de editare */
    private void addClientFields(EditFrame editFrame) {
        fields.add(editFrame.getFirstNameField());
        fields.add(editFrame.getLastNameField());
        fields.add(editFrame.getAddressField());
    }
    /** Adauga in fereastra de editare campurile necesare pentru editarea sau adaugarea unui produs.
     * @param editFrame Fereastra de editare */
    private void addProductFields(EditFrame editFrame) {
        fields.add(editFrame.getProductNameField());
        fields.add(editFrame.getOriginField());
    }
    /** Adauga in fereastra de editare campurile necesare pentru editarea unui stoc.
     * @param editFrame Fereastra de editare */
    private void addStockFields(EditFrame editFrame) throws SQLException {
        fields.add(editFrame.getProductQuantityField());
        fields.add(editFrame.getProductPriceField());

    }
    /** Adauga in fereastra de editare campurile necesare pentru plasarea unei comenzi.
     * @param editFrame Fereastra de editare */
    private void addOrderFields(EditFrame editFrame) {
        double productPrice = (Double)controller.getStockDAO().getRowWithIndex(editFrame.getOrderProductField()).get(2);
        fields.add(editFrame.getOrderClientField());
        fields.add(editFrame.getOrderProductField());
        fields.add(editFrame.getOrderQuantityField());
        fields.add(editFrame.getOrderQuantityField() * productPrice);
        System.out.println("client: " + editFrame.getOrderClientField());
        System.out.println("produs: " + editFrame.getOrderProductField());
        System.out.println("cantitate: " + editFrame.getOrderQuantityField());
        System.out.println("pret: " + productPrice);
    }

    /** Metoda utilitara pentru obtinerea indexului randului selectat din tabelul trimis ca parametru
     * @param table Tabelul dorit */
    private void getIndexValue(JTable table) { fields.add( (Integer)table.getValueAt(table.getRowCount() - 1, 0) + 1 ); }
    public enum TableType {CLIENTS_TABLE, PRODUCTS_TABLE, ORDERS_TABLE, STOCKS_TABLE}
}