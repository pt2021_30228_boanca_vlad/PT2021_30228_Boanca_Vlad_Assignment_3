package ro.tuc.pt.tema3.Presentation;

import ro.tuc.pt.tema3.DAO.ClientDAO;
import ro.tuc.pt.tema3.DAO.OrdersDAO;
import ro.tuc.pt.tema3.DAO.ProductDAO;
import ro.tuc.pt.tema3.DAO.StockDAO;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Face legatura intre interfata grafica si clasele care comunica cu baza de date
 */
public class Controller {
    /** Variabila instanta a clasei prin intermediul careia se fac operatii asupra tabelului "Client" din baza de date */
    private final ClientDAO clientDAO;
    /** Variabila instanta a clasei prin intermediul careia se fac operatii asupra tabelului "Product" din baza de date */
    private final ProductDAO productDAO;
    /** Variabila instanta a clasei prin intermediul careia se fac operatii asupra tabelului "Stock" din baza de date */
    private final StockDAO stockDAO;
    /** Variabila instanta a clasei prin intermediul careia se fac operatii asupra tabelului "Orders" din baza de date */
    private final OrdersDAO ordersDAO;
    /** Creaza un obiect instanta a clasei Controller */
    public Controller() {
        clientDAO = new ClientDAO();
        productDAO = new ProductDAO();
        ordersDAO = new OrdersDAO();
        stockDAO = new StockDAO();
    }

    /** Returneaza continutul unui anumit tabel din baza de date
     * @param arg Numele tabelului in baza de date
     * @return Continutul tabelului din baza de date sub forma unui obiect de tipul JTable
     * @throws SQLException
     * @throws IllegalAccessException */
    public JTable getTableFor(String arg) throws SQLException, IllegalAccessException {
        switch (arg) {
            case "Client": return clientDAO.createTable(clientDAO.viewAll());
            case "Product": return productDAO.createTable(productDAO.viewAll());
            case "Orders": return ordersDAO.createTable(ordersDAO.viewAll());
            case "Stock": return stockDAO.createTable(stockDAO.viewAll());
        }
        return null;
    }
    /** Returneaza un obiectul instanta a clasei ClientDAO
     * @return Obiect de tipul ClientDAO */
    public ClientDAO getClientDAO() { return clientDAO; }

    /** Returneaza un obiectul instanta a clasei ProductDAO
     * @return Obiect de tipul ProductDAO */
    public ProductDAO getProductDAO() { return productDAO; }

    /** Returneaza un obiectul instanta a clasei OrdersDAO
     * @return Obiect de tipul OrdersDAO */
    public OrdersDAO getOrdersDAO() { return ordersDAO; }

    /** Returneaza un obiectul instanta a clasei StockDAO
     * @return Obiect de tipul StockDAO */
    public StockDAO getStockDAO() { return stockDAO; }

    /** Genereaza un fisier text cu informatii despre comanda efectuata
     * @param editFrame Fereastra care contine informatiile comenzii introduse de utilizator
     * @throws IOException */
    public void generateBill(EditFrame editFrame) throws IOException {
        int ind = getOrdersDAO().getNumberOfRows();
        String fileName = "bill" + ind + ".txt";
        String delimiter = "--------------------------------------------------------------------\n";
        new File(fileName).delete();
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
        StringBuilder output = new StringBuilder();
        output.append(delimiter);
        output.append("Order ID: ").append(ind).append("\nClient: ").append(editFrame.getOrderClientName()).append("\nProduct: ");
        output.append(editFrame.getOrderProductName()).append("\nQuantity: ").append(editFrame.getOrderQuantityField());
        output.append("\nTotal price: ").append(editFrame.getOrderPriceField()).append("\n").append(delimiter);
        writer.append(output);
        writer.flush();
        writer.close();
    }
}