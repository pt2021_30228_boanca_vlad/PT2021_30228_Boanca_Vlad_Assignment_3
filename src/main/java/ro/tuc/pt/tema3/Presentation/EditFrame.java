package ro.tuc.pt.tema3.Presentation;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.pt.tema3.DAO.*;
import ro.tuc.pt.tema3.Model.Client;
import ro.tuc.pt.tema3.Model.Product;
import ro.tuc.pt.tema3.Model.Stock;

import javax.swing.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/** Clasa care extinde clasa JFrame. Creaza o fereastra cu campuri de editare, in functie de tabelul pentru care dorim sa efectuam editarea */
public class EditFrame extends JFrame {
    private JPanel panel;
    private JTextField firstNameField;
    private JTextField lastNameField;
    private JTextField addressField;

    private JTextField productNameField;
    private JTextField originField;

    private JTextField productQuantityField;
    private JTextField productPriceField;

    private JComboBox<String> orderClientField;
    private JComboBox<String> orderProductField;
    private JComboBox<Double> orderPriceField;
    private JTextField orderQuantityField;

    private JButton submitChanges;
    private ArrayList<Object> selectedRow;

    /** Creaza un obiect instanta a clasei EditFrame continand campurile necesare pentru editarea tabelului Client
     * @param index
     * @param clientDAO
     */
    public EditFrame(final Integer index, final ClientDAO clientDAO) {
        selectedRow = clientDAO.getRowWithIndex(index);
        setupEditClientFrame(selectedRow);
    }

    /** Creaza un obiect instanta a clasei EditFrame continand campurile necesare pentru editarea tabelului Produs
     * @param index
     * @param productDAO
     */
    public EditFrame(final Integer index, final ProductDAO productDAO) {
        selectedRow = productDAO.getRowWithIndex(index);
        setupEditProductFrame(selectedRow);
    }

    /** Creaza un obiect instanta a clasei EditFrame continand campurile necesare pentru editarea tabelului Stock
     * @param index
     * @param stockDAO
     */
    public EditFrame(final Integer index, final StockDAO stockDAO) {
        selectedRow = stockDAO.getRowWithIndex(index);
        setupEditStockFrame(selectedRow);
    }

    /** Creaza un obiect instanta a clasei EditFrame continand campurile necesare pentru editarea tabelului Orders
     * @param index
     * @param clientDAO
     * @param productDAO
     * @param stockDAO
     * @param ordersDAO
     * @throws SQLException
     */
    public EditFrame(final Integer index, final ClientDAO clientDAO, final ProductDAO productDAO, final StockDAO stockDAO, final OrdersDAO ordersDAO) throws SQLException {
        selectedRow = ordersDAO.getRowWithIndex(index);
        List clients = clientDAO.viewAll();
        List products = productDAO.viewAll();
        List stocks = stockDAO.viewAll();
        setupEditOrderFrame(selectedRow, clients, products, stocks);
    }

    /** Creaza un obiect instanta a clasei EditFrame continand campurile necesare pentru crearea unui nou client sau a unui nou produs.
     * @param abstractDAO
     */
    public EditFrame(final AbstractDAO abstractDAO) {
        switch (abstractDAO.getClass().getSimpleName()) {
            case "ClientDAO": setupEditClientFrame(null); break;
            case "ProductDAO": setupEditProductFrame(null); break;
        }
    }

    /** Returneaza un obiect din clasa JPanel continand campurile necesare editarii unui client.
     * @return Obiect din clasa JPanel
     */
    public JPanel createEditClientPanel() {
        FormLayout layout = new FormLayout(
                "right:pref, 8px, 100px, 4px, 200px",
                "pref, 6px, pref, 6px, pref, 6px, pref");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addLabel("First name", cc.xy (1,  1));
        builder.add(firstNameField, cc.xyw(3, 1, 3));
        builder.addLabel("Last name", cc.xy (1,  3));
        builder.add(lastNameField, cc.xyw(3, 3, 3));
        builder.addLabel("Address", cc.xy (1,  5));
        builder.add(addressField, cc.xyw(3, 5, 3));
        builder.add(submitChanges, cc.xyw(1, 7, 5));
        return builder.getPanel();
    }

    /** Returneaza un obiect din clasa JPanel continand campurile necesare editarii unui produs.
     * @return Obiect din clasa JPanel */
    public JPanel createEditProductPanel() {
        FormLayout layout = new FormLayout(
                "right:pref, 8px, 100px, 4px, 200px",
                "pref, 6px, pref, 6px, pref");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addLabel("Product name", cc.xy (1,  1));
        builder.add(productNameField, cc.xyw(3, 1, 3));
        builder.addLabel("Origin", cc.xy (1,  3));
        builder.add(originField, cc.xyw(3, 3, 3));
        builder.add(submitChanges, cc.xyw(1, 5, 5));
        return builder.getPanel();
    }

    /** Returneaza un obiect din clasa JPanel continand campurile necesare editarii stocului unui produs.
     * @return Obiect din clasa JPanel */
    public JPanel createEditStockPanel() {
        FormLayout layout = new FormLayout(
                "right:pref, 8px, 100px, 4px, 200px",
                "pref, 6px, pref, 6px, pref");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addLabel("Quantity", cc.xy(1, 1));
        builder.add(productQuantityField, cc.xyw(3, 1, 3));
        builder.addLabel("Product price", cc.xy(1, 3));
        builder.add(productPriceField, cc.xyw(3, 3, 3));
        builder.add(submitChanges, cc.xyw(1, 5, 5));
        return builder.getPanel();
    }

    /** Returneaza un obiect din clasa JPanel continand campurile necesare pentru adaugarea unei comenzi.
     * @return Obiect din clasa JPanel */
    public JPanel createEditOrderPanel() {
        FormLayout layout = new FormLayout(
                "right:pref, 8px, 100px, 4px, 200px",
                "pref, 6px, pref, 6px, pref, 6px, pref");
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addLabel("Product", cc.xy(1, 1));
        builder.add(orderProductField, cc.xyw(3, 1, 3));
        builder.addLabel("Ordered by", cc.xy(1, 3));
        builder.add(orderClientField, cc.xyw(3, 3, 3));
        builder.addLabel("Quantity", cc.xy(1, 5));
        builder.add(orderQuantityField, cc.xyw(3, 5, 3));
        builder.add(submitChanges, cc.xyw(1, 7, 5));
        return builder.getPanel();
    }

    /** Initializeaza corespunzator campurile de editare sau adaugare a unui client.
     * @param selectedClient Lista continand valorile pentru campurile clientului */
    public void setupEditClientFrame(ArrayList<Object> selectedClient) {
        if(selectedClient == null) {
            this.setTitle("Add new client");
            submitChanges = new JButton("Add client");
            firstNameField = new JTextField();
            lastNameField = new JTextField();
            addressField = new JTextField();
        } else {
            this.setTitle("Edit client properties");
            submitChanges = new JButton("Submit changes");
            firstNameField = new JTextField(selectedClient.get(1).toString());
            lastNameField = new JTextField(selectedClient.get(2).toString());
            addressField = new JTextField(selectedClient.get(3).toString());
        }
        panel = new JPanel();
        panel.add(createEditClientPanel());
        init();
    }

    /** Initializeaza corespunzator campurile de editare sau adaugare a unui produs.
     * @param selectedProduct Lista continand valori pentru campurile produsului */
    public void setupEditProductFrame(ArrayList<Object> selectedProduct) {
        if(selectedProduct == null) {
            this.setTitle("Add new product");
            submitChanges = new JButton("Add product");
            productNameField = new JTextField();
            originField = new JTextField();
        } else {
            this.setTitle("Edit product properties");
            submitChanges = new JButton("Submit changes");
            productNameField = new JTextField(selectedProduct.get(1).toString());
            originField = new JTextField(selectedProduct.get(2).toString());
        }
        panel = new JPanel();
        panel.add(createEditProductPanel());
        init();
    }

    /** Initializeaza corespunzator campurile de editare a stocului unui produs.
     * @param selectedStock Lista continand valori pentru campurile stocului */
    public void setupEditStockFrame(ArrayList<Object> selectedStock) {
        this.setTitle("Edit stock properties");
        submitChanges = new JButton("Submit changes");
        productQuantityField = new JTextField(selectedStock.get(1).toString());
        productPriceField = new JTextField(selectedStock.get(2).toString());
        panel = new JPanel();
        panel.add(createEditStockPanel());
        init();
    }

    /** Initializeaza corespunzator campurile utilizate pentru plasarea unei comenzi
     * @param selectedOrder Lista continand valori pentru campurile comenzii
     * @param clients Lista cu clientii existenti
     * @param products Lista cu produsele existente
     * @param stocks Lista cu stocul produselor */
    public void setupEditOrderFrame(ArrayList<Object> selectedOrder, List clients, List products, List stocks) {
        this.setTitle("Edit order properties");
        submitChanges = new JButton("Submit changes");
        orderClientField = new JComboBox<>();
        orderPriceField = new JComboBox<>();
        for (Object client : clients) {
            orderClientField.addItem(((Client)client).getFirstName() + " " + ((Client)client).getLastName());
        }
        orderProductField = new JComboBox<>();
        for (Object product : products) {
            orderProductField.addItem(((Product)product).getProductName());
        }
        for (Object stock : stocks) {
            orderPriceField.addItem(((Stock)stock).getPrice());
        }
        orderQuantityField = new JTextField();
        panel = new JPanel();
        panel.add(createEditOrderPanel());
        init();
    }

    /** Initializeaza Frame-ul interfetei grafice. */
    private void init() {
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(400, 200);
        this.setLocationRelativeTo(null);
        this.setContentPane(panel);
        this.setVisible(true);
    }

    public String getFirstNameField() { return firstNameField.getText(); }
    public String getLastNameField() { return lastNameField.getText(); }
    public String getAddressField() { return addressField.getText(); }
    public JButton getSubmitButton() { return submitChanges; }
    public String getProductNameField() { return productNameField.getText(); }
    public String getOriginField() { return originField.getText(); }
    public Double getProductPriceField() { return Double.valueOf(productPriceField.getText()); }
    public Integer getProductQuantityField() { return Integer.valueOf(productQuantityField.getText()); }
    public Integer getOrderQuantityField() { return Integer.valueOf(orderQuantityField.getText()); }
    public Integer getOrderClientField() { return orderClientField.getSelectedIndex() + 1; }
    public Integer getOrderProductField() { return orderProductField.getSelectedIndex() + 1; }
    public Double getOrderPriceField() { return orderPriceField.getItemAt(orderPriceField.getSelectedIndex() + 1); }
    public String getOrderClientName() { return orderClientField.getItemAt(orderClientField.getSelectedIndex()); }
    public String getOrderProductName() { return orderProductField.getItemAt(orderProductField.getSelectedIndex()); }
}