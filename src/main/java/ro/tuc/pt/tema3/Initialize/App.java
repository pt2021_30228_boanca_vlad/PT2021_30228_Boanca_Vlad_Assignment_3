package ro.tuc.pt.tema3.Initialize;

import ro.tuc.pt.tema3.Presentation.MainFrame;

import java.sql.*;

/** Contine metoda main a programului */
public class App  {
    /** Initializeaza interfata grafica a programului */
    public static void main( String[] args ) {
        try {
            new MainFrame();
        }catch (SQLException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }
}
