package ro.tuc.pt.tema3.DAO;

import ro.tuc.pt.tema3.BusinessLogic.*;
import ro.tuc.pt.tema3.Connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/** Furnizeaza metode pentru interactionarea cu un tabel din baza de date */
public class AbstractDAO<T>{
    /** Tine evidenta erorilor intalnite in rularea programului */
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    /** Asociaza indicele unui rand din tabel cu valorile coloanelor pentru acel randp*/
    protected HashMap<Integer, ArrayList<Object>> rows;
    /** Retine clasa propriu-zisa a genericului T*/
    private final Class<T> type;
    /** Valideaza inserarile si actualizarile din baza de date*/
    private Validator<T> validator;

    /** Creaza un obiect instanta a clasei AbstractDAO */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        rows = new HashMap<>();
        switch (type.getSimpleName()){
            case "Client": validator = (Validator<T>) new ClientValidator(); break;
            case "Product": validator = (Validator<T>) new ProductValidator(); break;
            case "Stock": validator = (Validator<T>) new StockValidator(); break;
            case "Orders": validator = (Validator<T>) new OrderValidator(); break;
        }
    }
    /** Creaza cu ajutorul unui ResultSet o lista de obiecte din clasa T
     * @param resultSet Obiect de tipul ResultSet care contine datele extrase din abelul corespunzator din baza de date
     * @return Lista de obiecte de tipul T, fiecare reprezentand o linie din tabelul corespunzator din baza de date*/
    private List<T> createObjectsFromResultSet(ResultSet resultSet) {
        ArrayList<T> objectList = new ArrayList<>();
        try {
            while(resultSet.next()) {
                T instance = type.newInstance();
                for(Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                objectList.add(instance);
            }
            return objectList;
        } catch (IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException | InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }
    /** Creaza un obiect din clasa T cu ajutorul unei liste de obiecte care reprezinta valorile pentru fiecare coloana a tabelului corespunzator din baza de date
     * @param args Lista de obiecte reprezentand valori pentru fiecare coloana din tabelul corespunzator din baza de date
     * @return Obiect din clasa T
     * @throws IllegalAccessException
     * @throws InstantiationException */
    private T createObjectFromArray(ArrayList<Object> args) throws IllegalAccessException, InstantiationException {
        T instance = type.newInstance();
        Field[] field = type.getDeclaredFields();
        try {
            for (int i = 0; i < field.length; i++) {
                Object value = args.get(i);
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field[i].getName(), type);
                Method method = propertyDescriptor.getWriteMethod();
                method.invoke(instance, value);
            }
        }catch (IntrospectionException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return instance;
    }
    /** Creaza interogare de tip SELECT pentru tabelul corespunzator din baza de date
     * @return Obiect din clasa String care contine interogarea */
    private String createSelectAllQuery() {
        return "SELECT * FROM " + type.getSimpleName() + ";";
    }
    /** Creaza interogare de tip SELECT dupa cheia primara a tabelului corespunzator
     * @return Obiect din clasa String care contine interogarea */
    private String createSelectByFieldQuery() {
        return "SELECT * FROM" + type.getSimpleName() + " WHERE " + type.getSimpleName().toLowerCase() + "Id = ? ";
    }
    /** Creaza interogare de tip INSERT pentru tabelul corespunzator din baza de date
     * @param object Obiectul pentru care se doreste inserarea
     * @return Obiect din clasa String care contine interogarea
     * @throws IllegalArgumentException
     * @throws IllegalAccessException */
    private String createInsertQuery(T object) throws IllegalArgumentException, IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        sb.append("INSERT INTO ").append(type.getSimpleName()).append(" VALUES (");
        for(Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if(!first)
                sb.append(", ");
            else
                first = false;
            if(field.get(object) instanceof Integer || field.get(object) instanceof Double)
                sb.append(field.get(object));
            else
                sb.append("'").append(field.get(object)).append("'");
        }
        return sb.append(");").toString();
    }
    /** Creaza interogare de tip UPDATE pentru tabelul corespunzator din baza de date
     * @param object Obiectul pentru care se face actualizarea
     * @return Obiect din clasa String care contine interogarea
     * @throws IllegalAccessException */
    private String createUpdateQuery(T object) throws IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(type.getSimpleName()).append(" SET ");
        Field[] field = object.getClass().getDeclaredFields();
        field[0].setAccessible(true);
        for(int i = 1; i < field.length; i++) {
            field[i].setAccessible(true);
            if(i >= 2)
                sb.append(", ");
            sb.append(field[i].getName()).append(" = ");
            if(field[i].get(object) instanceof Integer || field[i].get(object) instanceof Double)
                sb.append(field[i].get(object));
            else
                sb.append("'").append(field[i].get(object)).append("'");
        }
        sb.append(" WHERE ").append(type.getSimpleName().toLowerCase()).append("Id = ").append(field[0].get(object)).append(";").toString();
        return sb.toString();
    }
    /** Creaza interogare de tip DELETE pentru tabelul corespunzator din baza de date
     * @return Obiect din clasa String care contine interogarea*/
    private String createDeleteQuery() {
        return "DELETE FROM " + type.getSimpleName() + " WHERE " + type.getSimpleName().toLowerCase() + "Id = ?";
    }
    /** Creaza o lista de obiecte din clasa T care reprezinta toate intrarile din tabelul corespunzator din baza de date
     * @return Lista de obiecte de tipul T
     * @throws IllegalArgumentException
     * @throws SQLException */
    public List<T> viewAll() throws IllegalArgumentException, SQLException {
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement statement;
        String query = createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjectsFromResultSet(resultSet);
        } catch(SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO : ViewAll : " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    /** Adauga un obiect in tabelul corespunzator din baza de date
     * @param args Lista de obiecte reprexentand valorile pentru fiecare coloana a tabelului corespunzator din baza de date
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws SQLException */
    public void add(ArrayList<Object> args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, SQLException {
        T object = createObjectFromArray(args);
        validator.validate(object);
        String query = createInsertQuery(object);
        Connection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = ConnectionFactory.getConnection();
            statement = dbConnection.createStatement();
            statement.execute(query);
        } catch(SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO : add : " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }
    /** Actualizeaza campurile unui rand din tabelul corespunzator din baza de date
     * @param args Lista de obiecte reprezentand valorile pentru fiecare coloana din tabelul corespunzator din baza de date
     * @throws IllegalAccessException
     * @throws SQLException
     * @throws InstantiationException */
    public void update(ArrayList<Object> args) throws IllegalAccessException, SQLException, InstantiationException {
        T object = createObjectFromArray(args);
        validator.validate(object);
        String query = createUpdateQuery(object);
        Connection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = ConnectionFactory.getConnection();
            statement = dbConnection.createStatement();
            statement.executeUpdate(query);
        } catch(SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO : update : " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }
    /** Sterge un rand din tabelul corespunzator din baza de date
     * @param selectedClient Indicele randului care urmeaza sa fie sters
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException */
    public void delete(int selectedClient) throws InstantiationException, IllegalAccessException, SQLException {
        String query = createDeleteQuery();
        Connection dbConnection = null;
        PreparedStatement statement = null;
        try {
            dbConnection = ConnectionFactory.getConnection();
            statement = dbConnection.prepareStatement(query);
            statement.setInt(1, selectedClient);
            statement.executeUpdate();
        } catch(SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO : delete : " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(dbConnection);
        }
    }

    /** Creaza un Tabel cu informatiile din tabelul corespunzator din baza de date
     * @param objects Lista de obiecte de tip T reprezentand randurile din tabelul corespunzator din baza de date
     * @return Obiect de tipul JTable
     * @throws IllegalArgumentException
     * @throws IllegalAccessException */
    public JTable createTable(List<T> objects) throws IllegalArgumentException, IllegalAccessException {
        ArrayList<String> columnNamesArray = new ArrayList<>();
        for(Field field : objects.get(0).getClass().getDeclaredFields()) {
            field.setAccessible(true);
            columnNamesArray.add(field.getName());
        }
        String[] columnNames = new String[columnNamesArray.size()];
        columnNames = columnNamesArray.toArray(columnNames);
        DefaultTableModel dtm = new DefaultTableModel(columnNames, 0);
        for(Object object : objects) {
            ArrayList<Object> rowData = new ArrayList<>();
            for(Field field : object.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                rowData.add(field.get(object));
            }
            Object[] dataObjects = new Object[rowData.size()];
            rows.put((Integer)rowData.get(0), rowData);
            dataObjects = rowData.toArray(dataObjects);
            dtm.addRow(dataObjects);
        }
        return new JTable(dtm);
    }
    /** Obtine valorile coloanelor pentru tabelul corespunzator din baza de date pentru un anumit rand
     * @param index Indicele randului
     * @return */
    public ArrayList<Object> getRowWithIndex(Integer index) {
        return rows.get(index);
    }

    public Integer getNumberOfRows() {
        return rows.size();
    }
}
