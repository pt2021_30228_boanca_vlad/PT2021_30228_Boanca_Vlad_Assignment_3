package ro.tuc.pt.tema3.Connection;

import java.sql.*;

/** Efectueaza conexiunea la baza de date a programului */
public class ConnectionFactory {
    /** URL-ul pentru conectarea la baza de date */
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/tema3?serverTimezone=UTC";
    /** User-ul pentru baza de date */
    private static final String USER = "boanca";
    /** Parola pentru userul specificat. */
    private static final String PASS = "pass";
    /** Obiect de tipul Singleton*/
    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /** Creaza un obiect instanta a clasei ConnectionFactory */
    private ConnectionFactory() {}

    /** Creaza conexiuna la baza de date
     * @return Obiect din clasa Connection
     * @throws SQLException */
    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASS);
    }

    /** Returneaza conexiunea creata la baza de date
     * @return Obiect din clasa Connection
     * @throws SQLException */
    public static Connection getConnection() throws SQLException {
        return singleInstance.createConnection();
    }

    /** Termina o conexiune la baza de date
     * @param connection Obiect de tip Connection care reprezinta conxeiunea la o baza de date
     * @throws SQLException */
    public static void close(Connection connection) throws SQLException {
        connection.close();
    }

    /** Inchide un obiect de tipul Statement
     * @param statement Obiect de tipul Statement
     * @throws SQLException
     */
    public static void close(Statement statement) throws SQLException {
        statement.close();
    }

    /** Inchide un obiect de tipul ResultSet
     * @param resultSet Obiect de tipul ResultSet
     * @throws SQLException */
    public static void close(ResultSet resultSet) throws SQLException {
        resultSet.close();
    }
}
