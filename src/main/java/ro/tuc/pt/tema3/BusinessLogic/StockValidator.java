package ro.tuc.pt.tema3.BusinessLogic;

import ro.tuc.pt.tema3.Model.Stock;

/** Validator pentru stocurile produselor din baza de date*/
public class StockValidator implements Validator<Stock> {
    /** Valideaza atributele stocului inainte de introducerea in baza de date
     * @param stock Stocul care urmeaza sa fie introdus sau actualizat */
    @Override
    public void validate(Stock stock) {
        if(stock.getQuantity() < 0)
            throw new IllegalArgumentException("Invalid quantity");
        if(stock.getPrice() < 0)
            throw new IllegalArgumentException("Invalid price");
    }
}
