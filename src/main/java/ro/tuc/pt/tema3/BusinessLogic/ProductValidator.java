package ro.tuc.pt.tema3.BusinessLogic;

import ro.tuc.pt.tema3.Model.Product;

/** Validator pentru produsele introduse in baza de date*/
public class ProductValidator implements Validator<Product> {
    /** Valideaza atributele unui produs inainte de introducerea in baza de date
     * @param product Produsul care urmeaza sa fie introdus in baza de date */
    @Override
    public void validate(Product product) {
        if(product.getProductName().equals("") || !product.getProductName().matches("[a-zA-Z ]+"))
            throw new IllegalArgumentException("The product name must contain only letters and spaces");
        if(!product.getOrigin().matches("[a-zA-Z ]+"))
            throw new IllegalArgumentException("The origin must contain only letters and spaces");
    }
}
