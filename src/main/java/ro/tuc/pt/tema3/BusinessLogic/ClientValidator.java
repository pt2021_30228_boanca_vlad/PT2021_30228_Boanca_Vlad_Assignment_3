package ro.tuc.pt.tema3.BusinessLogic;

import ro.tuc.pt.tema3.Model.Client;

/** Validator pentru clientii introdusi in baza de date*/
public class ClientValidator implements Validator<Client> {
    /** Valideaza atributele unui client inainte de introducerea in baza de date
     * @param client Clientul care urmeaza sa fie introdus in baza de date*/
    @Override
    public void validate(Client client) {
        if(client.getFirstName().equals("") || !client.getFirstName().matches("[a-zA-Z ]+"))
            throw new IllegalArgumentException("The first name must contain only letters and spaces");
        if(client.getLastName().equals("") || !client.getLastName().matches("[a-zA-Z ]+"))
            throw new IllegalArgumentException("The last name must contain only letters and spaces");
    }
}
