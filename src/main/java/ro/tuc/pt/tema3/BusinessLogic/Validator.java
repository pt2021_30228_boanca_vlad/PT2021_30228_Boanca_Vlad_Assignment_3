package ro.tuc.pt.tema3.BusinessLogic;

/** Interfata implementata de validatorul unui obiect din clasa T
 * @param <T> Denumirea unei clase */
public interface Validator<T> {
    /** Valideaza atributele unui obiect inainte de introducerea in baza de date
     * @param t Obiect din clasa T*/
    public void validate(T t);
}
